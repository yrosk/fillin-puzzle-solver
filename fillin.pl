%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMP90048 Declarative Programming
% Semester 2, 2016
% Project 2 - Fillin Puzzle Solver
%
% Author: Skye Rajmon McLeman
% Date: 23/10/2016
%
% fillin.pl -- This file contains the main predicate and all helper
% predicates for the Fillin Puzzle Solver project. Peter Schachte's
% fillin_starter.pl file was used as a base.
%
% DESCRIPTION:
% This program solves fillin puzzles automatically. It takes as input
% a puzzle file and a list of words for that puzzle, then outputs
% the solution to a text file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODULES
% Ensures that the correct transpose/2 predicate is loaded.
:- ensure_loaded(library(clpfd)).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAIN

% Takes as input a fillin puzzle file and a list of words for that 
% puzzle, and outputs the solution to a text file.
main(PuzzleFile, WordlistFile, SolutionFile) :-
	read_file(PuzzleFile, Puzzle),
	read_file(WordlistFile, Wordlist),
	valid_puzzle(Puzzle),
	solve_puzzle(Puzzle, Wordlist, Solved),
	print_puzzle(SolutionFile, Solved).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% I/O: These predicates are used for reading and writing to files.

% Reads an entire file into memory.
% Used to read in both the unfilled puzzle file and the words list
% file for that puzzle.
read_file(Filename, Content) :-
	open(Filename, read, Stream),
	read_lines(Stream, Content),
	close(Stream).

% Reads in all lines of text from a file stream.
read_lines(Stream, Content) :-
	read_line(Stream, Line, Last),
	(   Last = true
	->  (   Line = []
	    ->  Content = []
	    ;   Content = [Line]
	    )
	;  Content = [Line|Content1],
	    read_lines(Stream, Content1)
	).

% Reads in a single line of text from a file stream.
read_line(Stream, Line, Last) :-
	get_char(Stream, Char),
	(   Char = end_of_file
	->  Line = [],
	    Last = true
	; Char = '\n'
	->  Line = [],
	    Last = false
	;   Line = [Char|Line1],
	    read_line(Stream, Line1, Last)
	).

% Prints the given puzzle solution to a file.
print_puzzle(SolutionFile, Puzzle) :-
	open(SolutionFile, write, Stream),
	maplist(print_row(Stream), Puzzle),
	close(Stream).

% Prints a row of the puzzle to the given stream.
print_row(Stream, Row) :-
	maplist(put_puzzle_char(Stream), Row),
	nl(Stream).

% Outputs a character from the puzzle to the given stream.
put_puzzle_char(Stream, Char) :-
	(   var(Char)
	->  put_char(Stream, '_')
	;   put_char(Stream, Char)
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUXILIARY: These auxiliary predicates are used by the I/O predicates.

% Holds if the puzzle is valid, i.e. each row has the
% same length.
valid_puzzle([]).
valid_puzzle([Row|Rows]) :-
	maplist(samelength(Row), Rows).

% Holds if two lists are of the same length.
samelength([], []).
samelength([_|L1], [_|L2]) :-
	same_length(L1, L2).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUZZLE SOLVING: These predicates are used for solving the fillin puzzle
% and make up the core of the program.

% Holds when Puzzle is a solved version of Puzzle0, with the
% empty slots filled in with words from WordList.  Puzzle0 and Puzzle
% should be lists of lists of characters (single-character atoms), one
% list per puzzle row.  WordList is also a list of lists of
% characters, one list per word.
solve_puzzle(Puzzle0, WordsList, Puzzle) :-
    fill_vars_puzzle(Puzzle0, Puzzle),
    wrap_vars_puzzle(Puzzle, PuzzleWrapped),
    transpose(PuzzleWrapped, PuzzleWrappedTransposed),
    collect_slots_puzzle(PuzzleWrapped, HorizontalSlotsWrapped),
    collect_slots_puzzle(PuzzleWrappedTransposed, VerticalSlotsWrapped),
    append(HorizontalSlotsWrapped, VerticalSlotsWrapped, AllSlotsWrapped),
    flatten_slot_list(AllSlotsWrapped, Flattened),
    unwrap_vars_all_slots(Flattened, AllSlotsUnwrapped),
    get_slot_matches_all(AllSlotsUnwrapped, WordsList, MatchList),
    sort(MatchList, SortedMatchList),
    fill_slots(SortedMatchList).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FILLING PUZZLE WITH UNBOUND VARIABLES

% Fills a '_' character with an unbound variable. Any other character
% is left as it is.
fill_var('_', _Z).
fill_var(X, X) :-
    X \= '_'.

% Fills all the '_' characters in a row with unbound variables.
% Letters and '#' characters are left alone.
fill_vars_row(Row, VarsFilledRow) :-
    maplist(fill_var, Row, VarsFilledRow).

% Fills all the '_' characters in the puzzle with unbound variables.
fill_vars_puzzle(Rows, Assigned) :-
    maplist(fill_vars_row, Rows, Assigned).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WRAPPING UNBOUND VARIABLES WITH FUNCTORS

% If X is an unbound variable or a letter, wraps it in a functor.
% '#' is replaced with the atom 'solid'.
wrap_var(X, Y) :-
    (   var(X) ->
        Y = fill(X)
    ;   X = # ->
        Y = solid
    ;   X = V ->
        Y = fill(V)
    ).

% Wraps all the unbound variables and letters in a row with functors.
wrap_vars_row(Row, VarsWrappedRow) :-
    maplist(wrap_var, Row, VarsWrappedRow).

% Wraps all the unbound variables (and letters) in the puzzle
% with functors.
% This allows us to manipulate the variables without having
% Prolog automatically unify them.
wrap_vars_puzzle(Rows, RowsWrapped) :-
    maplist(wrap_vars_row, Rows, RowsWrapped).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLLECTING UP SLOTS

% Splits a list using specified delimiter.
% This predicate is used when collecting up slots from each
% row.
split_list(List, Delim, [L|Ls]) :-
    append([L, [Delim], T], List),
    \+member(Delim, L),
    split_list(T, Delim, Ls).
split_list(List, Delim, [List]) :-
    \+member(Delim, List).

% True if a slot is not valid, i.e. has a length less than 2.
% Used in the collect_slots_row predicate to exclude
% invalid slots.
invalid_slot(Slot) :-
    length(Slot, Len),
    Len < 2.

% Collects up all the slots for a single row.
collect_slots_row(Row, Slots) :-
    split_list(Row, solid, Split),
    exclude(invalid_slot(), Split, Slots).

% Collects up all the slots for each row in the puzzle.
% Note that this will result in a list of lists of
% slots (which are also lists) for each row, which would be
% cumbersome to work with; hence the predicate
% flatten_slot_list that follows.
collect_slots_puzzle(Rows, Slots) :-
    maplist(collect_slots_row, Rows, Slots).

% Flattens the list of slots so that we have one long
% list of slots for the entire puzzle, rather than a list of
% the list of slots for each row.
flatten_slot_list([], []).
flatten_slot_list([[Slot|Slots]|Rows], [Slot|AllSlotsWrapped]) :-
    flatten_slot_list([Slots|Rows], AllSlotsWrapped).
flatten_slot_list([[]|Rows], AllSlotsWrapped) :-
    flatten_slot_list(Rows, AllSlotsWrapped).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UNWRAPPING VARIABLES IN SLOTS
    
% Extracts the unbound variables from the functors in a slot.
unwrap_vars_slot([], []). 
unwrap_vars_slot([fill(X)|Xs], [X|Ys]) :-
    unwrap_vars_slot(Xs, Ys).

% Extracts the unbound variables from the functors in every
% slot in the given list.
unwrap_vars_all_slots(Slots, Removed) :-
    maplist(unwrap_vars_slot, Slots, Removed).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FILLING SLOTS

% Produces a compound term containing the given slot, a list of the matches
% from the word list for that slot, and the number of those matches.
% The first argument of the functor is the number of matches so that
% we can easily sort a list of them later on.
get_slot_matches(WordsList, Slot, slotmatches(NumMatches, Slot, Matches)) :-
    setof(Slot, member(Slot, WordsList), Matches),
    length(Matches, NumMatches).

% Produces a list of compound terms as described in the predicate 
% get_slot_matches for every slot in the given list.
get_slot_matches_all(Slots, WordsList, Matches) :-
    maplist(get_slot_matches(WordsList), Slots, Matches).

% Removes the given word from the list of matches for a single slot.
remove_word_matchlist(Word, slotmatches(_NumMatches, Slot, Matches),
    slotmatches(NumRemaining, Slot, Remaining)) :-
        delete(Matches, Word, Remaining),
        length(Remaining, NumRemaining).

% Removes the given word from the list of matches for every slot in the
% list.
remove_word_matchlist_all(MatchesList, Word, NewMatchesList) :-
    maplist(remove_word_matchlist(Word), MatchesList, NewMatchesList).

% Updates the list of possible matches for a single slot.
update_matches_single(slotmatches(_NumMatches, Slot, Matches), 
    slotmatches(NewNumMatches, Slot, UpdatedMatches)) :-
        setof(Slot, member(Slot, Matches), UpdatedMatches),
        length(UpdatedMatches, NewNumMatches).

% Updates the list of possible matches for every slot in the list.
update_matches_all(SlotmatchesList, UpdatedList) :-
    maplist(update_matches_single, SlotmatchesList, UpdatedList).

% Matches each slot in the list to a word from the words list.
% After each match, it removes the corresponding word from the list
% of possible matches for each slot, and updates these lists
% so that only legitimate matches remain. The remaining slots
% are sorted in ascending order based on the number of potential 
% matches they have so that the next slot to the filled has
% the fewest potential matches, thereby reducing the amount
% of backtracking required.
fill_slots([]).
fill_slots([SlotMatch|SlotMatches]) :-
    arg(2, SlotMatch, Slot),
    arg(3, SlotMatch, Matches),
    select(Slot, Matches, _),
    remove_word_matchlist_all([SlotMatch|SlotMatches],
        Slot, [_NewMatch|NewMatches]),
    update_matches_all(NewMatches, UpdatedMatches),
    sort(UpdatedMatches, UpdatedMatchesSorted),
    fill_slots(UpdatedMatchesSorted).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
